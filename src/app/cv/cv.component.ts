import { Candidats } from './../models/candidats';
import { Component } from '@angular/core';
import { ListCandidatService } from '../services/list-candidat.service';
import { FirstService } from '../services/first.service';

@Component({
  selector: 'app-cv',
  templateUrl: './cv.component.html',
  styleUrls: ['./cv.component.css'],
})
export class CvComponent {
  selectedCandidat: Candidats;

  listeCv: Candidats[] = [];

  constructor(
    private listeCandidats: ListCandidatService,
    private firstService: FirstService
  ) {}

  ngOnInit(): void {
    this.firstService.firstInfo();
  }

  getSelectedCandidat(candidat) {
    this.selectedCandidat = candidat;
  }

  addNewCandidat() {
    this.listeCandidats.addCandidat();
  }

  showList() {
    console.log(this.listeCandidats.getAllCandidats());
  }
}
