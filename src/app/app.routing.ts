import { CvComponent } from './cv/cv.component';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { DirectComponent } from './exo3/direct/direct.component';
import { TexteComponent } from './word/texte/texte.component';
import { InformationsComponent } from './informations/informations.component';

const myRoutes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: 'cv',
    component: CvComponent,
  },
  {
    path: 'cv/:id',
    component: InformationsComponent,
  },
  {
    path: 'server',
    component: DirectComponent,
  },
  {
    path: 'texte',
    component: TexteComponent,
  },
];

export const ORSYS_ROUTING = RouterModule.forRoot(myRoutes);
