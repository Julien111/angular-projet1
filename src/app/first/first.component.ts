import { Component } from '@angular/core';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css'],
})
export class FirstComponent {
  name: string = 'Jules';
  formation = 'Angular';
  color = 'yellow';
  hd = false;

  clickHandler() {
    alert("J'ai cliqué !");
  }
}
