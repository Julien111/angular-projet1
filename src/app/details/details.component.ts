import { Input } from '@angular/core';
import { Component } from '@angular/core';
import { Candidats } from '../models/candidats';
import { RecrutementService } from '../services/recrutement.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css'],
})
export class DetailsComponent {
  @Input() theCandidat: Candidats;

  constructor(private recrutement: RecrutementService) {}

  message: string;

  addRecrue(user) {
    this.message = this.recrutement.addCandidat(user);
  }
}
