import { ThisReceiver } from '@angular/compiler';
import { Component } from '@angular/core';

@Component({
  selector: 'app-direct',
  templateUrl: './direct.component.html',
  styleUrls: ['./direct.component.css'],
})
export class DirectComponent {
  listeServeurs = [
    {
      nom: 'Production Server',
      type: 'small',
      demarrage: 'FRI April',
      date: 'Jul 2020',
      statut: 'on',
    },
    {
      nom: 'Testing Server',
      type: 'medium',
      demarrage: 'FRI April',
      date: 'Jul 2020',
      statut: 'stand',
    },
    {
      nom: 'Development Server',
      type: 'large',
      demarrage: 'FRI April',
      date: 'Jul 2020',
      statut: 'off',
    },
    {
      nom: 'Server partie Search',
      type: 'large',
      demarrage: 'FRI DEC',
      date: 'Sep 2022',
      statut: 'on',
    },
  ];

  selectedStatut = '';

  class1 = '';

  changeSelectedStatut(value) {
    this.selectedStatut = value;
  }

  isAvailable(statut) {
    if (statut === 'on') {
      this.class1 = 'c1';
      return this.class1;
    } else if (statut === 'off') {
      this.class1 = 'c2';
      return this.class1;
    } else if (statut === 'stand') {
      this.class1 = 'c3';
      return this.class1;
    } else {
      this.class1 = '';
      return this.class1;
    }
  }

  // add Server

  addServer() {
    this.listeServeurs.push({
      nom: 'Server New Commerce',
      type: 'small',
      demarrage: 'FRI Nov',
      date: 'Sep 2022',
      statut: 'on',
    });
  }
}
