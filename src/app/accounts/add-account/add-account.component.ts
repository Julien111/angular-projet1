import { Component, EventEmitter } from '@angular/core';
import { Comptes } from 'src/app/models/comptes';
import { NgForm } from '@angular/forms';
import { Output } from '@angular/core';

@Component({
  selector: 'app-add-account',
  templateUrl: './add-account.component.html',
  styleUrls: ['./add-account.component.css'],
})
export class AddAccountComponent {
  unCompte: Comptes;

  @Output() details = new EventEmitter();

  onSubmit(f: NgForm) {
    console.log(f.value); // { first: '', last: '' }
    console.log(f.valid); // false
    this.unCompte = new Comptes(f.value.nom, f.value.statut);
    console.log(this.unCompte);
    this.details.emit(this.unCompte);
  }
}
