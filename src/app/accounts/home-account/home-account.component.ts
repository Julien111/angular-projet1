import { Component, EventEmitter, Output } from '@angular/core';
import { Comptes } from 'src/app/models/comptes';

@Component({
  selector: 'app-home-account',
  templateUrl: './home-account.component.html',
  styleUrls: ['./home-account.component.css'],
})
export class HomeAccountComponent {
  listeComptes: Comptes[] = [
    new Comptes('Compte 1', 'actif'),
    new Comptes('Compte 2', 'inactif'),
  ];

  @Output() msgToCv = new EventEmitter();

  sendToC(compte) {
    this.listeComptes.push(compte);
  }
}
