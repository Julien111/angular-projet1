import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter',
  pure: false,
})
export class FilterPipe implements PipeTransform {
  transform(value: any[], selectedStatut: string): any[] {
    if (selectedStatut.length === 0) {
      return value;
    } else {
      let newTab = [];
      for (let serv of value) {
        if (serv['statut'] == selectedStatut) {
          newTab.push(serv);
        }
      }
      return newTab;
    }
  }
}
