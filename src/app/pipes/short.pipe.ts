import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'short',
})
export class ShortPipe implements PipeTransform {
  transform(value: string, max: number): string {
    if (value.length < max) {
      return value;
    } else {
      return value.substring(0, max) + '...';
    }
  }
}
