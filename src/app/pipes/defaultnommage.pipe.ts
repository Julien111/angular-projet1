import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'defaultnommage',
})
export class DefaultnommagePipe implements PipeTransform {
  transform(value: string): string {
    if (value.length === 0) {
      return 'default-img.jpg';
    } else {
      return value;
    }
  }
}
