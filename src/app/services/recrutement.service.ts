import { Injectable } from '@angular/core';
import { Candidats } from '../models/candidats';

@Injectable({
  providedIn: 'root',
})
export class RecrutementService {
  private listeCand: Candidats[] = [];

  constructor() {}

  getAllCandidats() {
    return this.listeCand;
  }

  addCandidat(candidat: Candidats): string {
    for (let user of this.listeCand) {
      if (user.id === candidat.id) {
        return 'Le candidat est déjà dans la liste';
      }
    }
    this.listeCand.push(candidat);
    return 'Ok, le candidat est ajouté.';
  }
}
