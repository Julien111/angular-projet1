import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class FirstService {
  constructor() {}

  firstInfo() {
    console.log('Le premier service');
  }
}
