import { Injectable } from '@angular/core';
import { Candidats } from '../models/candidats';

@Injectable({
  providedIn: 'root',
})
export class ListCandidatService {
  private listeCv: Candidats[] = [
    new Candidats(1, 'Dan', 'Olivier', 30, 'Agent', 'avatar_m.png'),
    new Candidats(2, 'Vallecano', 'Marc', 32, 'Agent', 'avatar_m.png'),
    new Candidats(3, 'Duerte', 'Jules', 29, 'Pompier', ''),
  ];

  getAllCandidats() {
    return this.listeCv;
  }

  addCandidat() {
    this.listeCv.push(
      new Candidats(3, 'Duerte', 'Boris', 29, 'Médecin', 'avatar_m.png')
    );
  }

  constructor() {}
}
