import { Candidats } from './../models/candidats';
import {
  Component,
  EventEmitter,
  Input,
  Output,
  HostListener,
} from '@angular/core';

@Component({
  selector: 'app-item-one',
  templateUrl: './item-one.component.html',
  styleUrls: ['./item-one.component.css'],
})
export class ItemOneComponent {
  @Input() candidat: Candidats;
  @Output() msgToDetails = new EventEmitter();

  styleExp;
  colorBack = true;

  sendDetailToList() {
    this.msgToDetails.emit(this.candidat);
  }

  @HostListener('mouseenter') mouseenter() {
    this.styleExp = '#555';
  }

  @HostListener('mouseleave') mouseleave() {
    this.styleExp = 'white';
  }
}
