import { ORSYS_ROUTING } from './app.routing';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FirstComponent } from './first/first.component';
import { SecondComponent } from './second/second.component';
import { ThirdComponent } from './third/third.component';
import { CvComponent } from './cv/cv.component';
import { ItemOneComponent } from './item-one/item-one.component';
import { DetailsComponent } from './details/details.component';
import { ListComponent } from './list/list.component';
import { ListAccountComponent } from './accounts/list-account/list-account.component';
import { HomeAccountComponent } from './accounts/home-account/home-account.component';
import { AddAccountComponent } from './accounts/add-account/add-account.component';
import { TexteComponent } from './word/texte/texte.component';
import { DirectComponent } from './exo3/direct/direct.component';
import { CustomDirDirective } from './custom-dir.directive';
import { ShortPipe } from './pipes/short.pipe';
import { DefaultnommagePipe } from './pipes/defaultnommage.pipe';
import { FilterPipe } from './pipes/filter.pipe';
import { ListeRecrutementComponent } from './liste-recrutement/liste-recrutement.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { InformationsComponent } from './informations/informations.component';

@NgModule({
  declarations: [
    AppComponent,
    FirstComponent,
    SecondComponent,
    ThirdComponent,
    CvComponent,
    ItemOneComponent,
    DetailsComponent,
    ListComponent,
    ListAccountComponent,
    HomeAccountComponent,
    AddAccountComponent,
    TexteComponent,
    DirectComponent,
    CustomDirDirective,
    ShortPipe,
    DefaultnommagePipe,
    FilterPipe,
    ListeRecrutementComponent,
    HomeComponent,
    NavbarComponent,
    InformationsComponent,
  ],
  imports: [BrowserModule, FormsModule, ORSYS_ROUTING],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
