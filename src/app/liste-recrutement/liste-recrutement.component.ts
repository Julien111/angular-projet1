import { Component } from '@angular/core';
import { RecrutementService } from '../services/recrutement.service';

@Component({
  selector: 'app-liste-recrutement',
  templateUrl: './liste-recrutement.component.html',
  styleUrls: ['./liste-recrutement.component.css'],
})
export class ListeRecrutementComponent {
  constructor(private recrutement: RecrutementService) {}

  allCandidats = this.recrutement.getAllCandidats();
}
