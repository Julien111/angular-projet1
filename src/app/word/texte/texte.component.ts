import { Component } from '@angular/core';

@Component({
  selector: 'app-texte',
  templateUrl: './texte.component.html',
  styleUrls: ['./texte.component.css'],
})
export class TexteComponent {
  color;
  fontFamily;
  size;
  background;

  listeFonts = ['verdana', 'sans-serif', 'courrier', 'arial'];

  changeSize(inputSize) {
    this.size = inputSize.value + 'px';
  }
}
