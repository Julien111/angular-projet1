import { Candidats } from './../models/candidats';
import {
  Component,
  EventEmitter,
  Input,
  Output,
  HostListener,
} from '@angular/core';
import { ListCandidatService } from '../services/list-candidat.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent {
  @Input() allCandidats: Candidats[];
  @Output() msgToCv = new EventEmitter();
  styleExp;

  constructor(private listSer: ListCandidatService) {}

  ngOnInit(): void {
    this.allCandidats = this.listSer.getAllCandidats();
  }

  showList() {
    console.log(this.listSer.getAllCandidats());
  }

  sendCandToCv(candidat) {
    this.msgToCv.emit(candidat);
  }

  @HostListener('mouseenter') mouseenter() {
    this.styleExp = 'Arial';
  }

  @HostListener('mouseleave') mouseleave() {
    this.styleExp = 'verdana';
  }
}
