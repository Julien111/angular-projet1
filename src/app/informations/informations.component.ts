import { Component } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-informations',
  templateUrl: './informations.component.html',
  styleUrls: ['./informations.component.css'],
})
export class InformationsComponent {
  showId;
  constructor(private activatedRoute: ActivatedRoute) {}

  // les observables

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe({
      next: (p: ParamMap) => {
        this.showId = p.get('myid');
      },
    });
  }
}
