// export class Candidats {
//   id: number;
//   prenom: string;
//   nom: string;

//   constructor(id: number, prenom: string, nom: string) {
//     this.id = id;
//     this.prenom = prenom;
//     this.nom = nom;
//   }
// }
export class Candidats {
  constructor(
    public id: number,
    public nom: string,
    public prenom: string,
    public age: number,
    public profession: string,
    public avatar: string
  ) {}
}
